# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase
from django.core.urlresolvers import reverse
from models import Board
# from django.urls import resolve
# from views import BoardsView
# Create your tests here.


class BoardListTest(TestCase):
    def test_home_view_status_code(self):
        url = reverse('boards:index')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    # def test_home_url_resolve_home_view(self):
    #     view = resolve('/boards/')
    #     self.assertEqual(view.func, BoardsView)


class BoardTopicsTests(TestCase):
    def setUp(self):
        Board.objects.create(name='Django', description='Django Board')

    def test_board_topic_view_success_status_code(self):
        url = reverse('boards:detail', kwargs={'pk': 1})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_board_topic_view_not_found_status_code(self):
        url = reverse('boards:detail', kwargs={'pk': 99})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)
