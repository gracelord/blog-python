# -*- coding: utf-8 -*-
# from __future__ import unicode_literals
from django.http import HttpResponseBadRequest, Http404
from django.shortcuts import get_object_or_404, render, redirect
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views import generic
from boards.models import Board, Topic, Post
from django.contrib.auth.models import User
from .forms import NewTopicForm, PostForm
from django.contrib.auth.decorators import login_required
from django.db.models import Count
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
# Create your views here.
import pdb


class BoardsView(generic.ListView):
    template_name = 'boards/index.html'
    context_object_name = 'boards'
    model = Board


def home(request):
    boards = Board.objects.all()
    return render(request, 'boards/index.html', {'boards': boards})


class BoardDetail(generic.ListView):
    model = Topic
    template_name = 'boards/detail.html'
    context_object_name = 'topics'
    paginate_by = 5

    def get_context_data(self, **kwargs):
        kwargs['board'] = self.board
        return super(BoardDetail, self).get_context_data(**kwargs)

    def get_queryset(self):
        self.board = get_object_or_404(Board, pk=self.kwargs.get('pk'))
        queryset = self.board.topics.order_by('-last_updated').annotate(replies=Count('posts') - 1)
        return queryset


def board_topics(request, pk):
    # try:
    #     board = Board.objects.get(pk=pk)
    # except Board.DoesNotExist:
    #     raise Http404
    board = get_object_or_404(Board, pk=pk)
    queryset = board.topics.order_by('-last_updated').annotate(replies=Count('posts') - 1)
    page = request.GET.get('page', 1)
    paginator = Paginator(queryset, 5)
    try:
        topics = paginator.page(page)
    except PageNotAnInteger:
        topics = paginator.page(1)
    except EmptyPage:
        topics = paginator.page(paginator.num_pages)

    return render(request, 'boards/detail.html', {'board': board, 'topics': topics})


class NewTopic(generic.DetailView):
    model = Board
    template_name = 'boards/new_topic.html'

    def post(self, request, **kwargs):
        # pdb.set_trace()
        pk = kwargs.get('pk')
        board = get_object_or_404(Board, pk=pk)
        user = User.objects.first()
        form = NewTopicForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('boards:detail', pk=board.pk)
        else:
            return render(request, 'boards/new_topic.html', {
                'board': board,
                'error_message': "Invalid"
            })
        # try:
        #     subject = request.POST['subject']
        #     message = request.POST['message']
        #
        #     topic = Topic.objects.create(
        #         subject=subject,
        #         board=board,
        #         starter=user
        #     )
        #
        #     post = Post.objects.create(
        #         message=message,
        #         created_by=user,
        #         topic=topic
        #     )
        # except KeyError as e:
        #     return render(request, 'boards/new_topic.html', {
        #         'board': board,
        #         'error_message': "Missing POST parameters {}".format(e)
        #     })
        #
        # return redirect('boards:detail', pk=board.pk)


@login_required
def new_topic(request, pk):
    board = get_object_or_404(Board, pk=pk)
    user = request.user
    if request.method == 'POST':
        form = NewTopicForm(request.POST)
        if form.is_valid():
            topic = form.save(commit=False)
            topic.board = board
            topic.starter = user
            topic.save()
            post = Post.objects.create(
                message=form.cleaned_data.get('message'),
                topic=topic,
                created_by=user
            )
            return redirect('boards:topic_posts', pk=board.pk, topic_pk=topic.pk)
    else:
        form = NewTopicForm()

    return render(request, 'boards/new_topic.html', {'form': form, 'board': board})


@login_required
def topic_posts(request, pk, topic_pk):
    topic = get_object_or_404(Topic, board__pk=pk, pk=topic_pk)
    topic.views += 1
    topic.save()
    return render(request, 'boards/topic_posts.html', {'topic': topic})


@login_required
def reply_topic(request, pk, topic_pk):
    topic = get_object_or_404(Topic, board__pk=pk, pk=topic_pk)
    if request.method == 'POST':
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.topic = topic
            post.created_by = request.user
            post.save()
            return redirect('boards:topic_posts', pk=pk, topic_pk=topic_pk)
    else:
        form = PostForm()

    return render(request, 'boards/reply_topic.html', {'form': form, 'topic': topic})


@method_decorator(login_required, name='dispatch')
class PostUpdateView(generic.UpdateView):
    model = Post
    fields = ('message', )
    template_name = 'boards/edit_post.html'
    pk_url_kwarg = 'post_pk'
    context_object_name = 'post'

    def get_queryset(self):
        queryset = super(PostUpdateView, self).get_queryset()
        return queryset.filter(created_by=self.request.user)

    def form_valid(self, form):
        post = form.save(commit=False)
        post.updated_by = self.request.user
        post.updated_at = timezone.now()
        post.save()

        return redirect('boards:topic_posts', pk=post.topic.board.pk, topic_pk=post.topic.pk)