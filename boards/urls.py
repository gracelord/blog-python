from django.conf.urls import url
from boards import views

app_name = 'boards'
urlpatterns = [
    # url(r'^$', views.BoardsView.as_view(), name='index'),
    url(r'^$', views.BoardsView.as_view(), name='index'),
    # url(r'^(?P<pk>[0-9]+)/$', views.BoardDetail.as_view(), name='detail'),
    url(r'^(?P<pk>[0-9]+)/$', views.BoardDetail.as_view(), name='detail'),
    # url(r'^(?P<pk>[0-9]+)/new/$', views.NewTopic.as_view(), name='new_topic')
    url(r'^(?P<pk>[0-9]+)/new/$', views.new_topic, name='new_topic'),
    url(r'^(?P<pk>[0-9]+)/topic/(?P<topic_pk>\d+)$', views.topic_posts, name='topic_posts'),
    url(r'^(?P<pk>[0-9]+)/topic/(?P<topic_pk>\d+)/reply/$', views.reply_topic, name='reply_topic'),
    url(r'^(?P<pk>\d+)/topics/(?P<topic_pk>\d+)/posts/(?P<post_pk>\d+)/edit/$',
        views.PostUpdateView.as_view(), name='edit_post'),
]